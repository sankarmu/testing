FROM microsoft/dotnet:2.2-sdk

FROM microsoft/dotnet:2.2-aspnetcore-runtime

WORKDIR /app

COPY /out .

ENV ASPNETCORE_URLS=http://*:8081

EXPOSE 8081

ENTRYPOINT ["dotnet", "hellobenzy.dll"]  